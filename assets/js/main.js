var Cart = function() {
    this.$cart = $("#my-cart");
    this.cartItems = [];

    var self = this;
    this.init = function(){
        //create a new CartItem object for each .cart-item element.
        this.$cart.find('.cart-item').each(function(){
            var cartItem = new CartItem($(this), self, self.cartItems.length);
            self.cartItems.push(cartItem);
        });
        //create a new instance of CartTotals and pass it this object.
        this.CartTotals = new CartTotals(this);
    };
    this.removeItem = function(cartItem) {
        this.cartItems.splice(cartItem.id, 1);
        cartItem.$element.remove();
        this.$cart.trigger('item_removed');
    };

    this.init();
};

var CartItem = function($cartItemElement, cart, id) {
    this.$element = $cartItemElement;
    this.id = id;
    this.$priceElement = $cartItemElement.find(".cart-item-price");
    this.price = this.$priceElement.data('price');
    this.$qtySelector = $cartItemElement.find(".cart-item-qty-selector");
    this.qty = this.$qtySelector.val();
    this.$removebutton = $cartItemElement.find('.cart-item-remove');

    var self = this;
    this.init = function() {
        this.$removebutton.click(function(){
            cart.removeItem(self);
        });
        this.$qtySelector.on('change',function(){
            self.qty = self.$qtySelector.val();
            self.$element.trigger("quantity_changed");
        })
    };

    this.getItemSubtotal = function() {
        return this.qty * this.price;
    };

    this.init();
};

var CartTotals = function(cart){
    this.cart = cart;
    this.subtotal = 0;
    this.$subtotalElement = cart.$cart.find(".cart-summary-subtotal .summary-value");
    this.tax = 0;
    this.$taxElement = cart.$cart.find(".cart-summary-tax .summary-value");
    this.total = 0;
    this.$totalElement = cart.$cart.find(".cart-summary-total .summary-value");
    this.taxRate = .10;
    var self = this;
    this.init = function(){
        this.calculateSubtotal();
        this.calculateTax();
        this.calculateTotal();
        $(document).on('quantity_changed item_removed', function(){
            self.updateTotals();
        });
    };
    this.formatCurrency = function(amount) {
        amount = Number(amount);
        amount = amount.toFixed(2);
        console.log(amount);
        console.log(typeof parseFloat(amount));
        amount = "$" + amount;
        return amount;
    };
    this.calculateSubtotal = function() {
        var subtotal = 0;
        this.cart.cartItems.forEach(function(cartItem){
            subtotal = subtotal + cartItem.getItemSubtotal();
        });
        this.subtotal = subtotal;
        return this.formatCurrency(this.subtotal);
    };
    this.calculateTax = function() {
        this.tax = this.subtotal * this.taxRate;
        return this.formatCurrency(this.tax);
    };
    this.calculateTotal = function() {
        this.total = this.subtotal + this.tax;
        return this.formatCurrency(this.total);
    };
    this.updateTotals = function(){
        this.$subtotalElement.html(this.calculateSubtotal());
        this.$taxElement.html(this.calculateTax());
        this.$totalElement.html(this.calculateTotal());
    };

    this.init();
};

var MyCart = new Cart();